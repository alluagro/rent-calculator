var calculateRent = require('./../src/calculate-rent')

describe('Testando Tipo ' + calculateRent.typesPricing.DAILY, function () {
	test('Calcular sem Frete', function() {
		var machine = buildMachineDaily()
		var rent = buildRent()
		var finalValue = calculateRent.calculateRentValue(machine, rent.date.start, rent.date.end, rent.price.value,
			rent.price.value, machine.distance, rent.price.packageIndex)

		expect(finalValue.total).toBe(50)
		expect(finalValue.freight).toBe(0)
		expect(finalValue.subTotal).toBe(50)
	})

	test('Calcular com Frete Sem atingir o mínimo', function() {
		var machine = buildMachineDaily(true)
		var rent = buildRent()
		var finalValue = calculateRent.calculateRentValue(machine, rent.date.start, rent.date.end, rent.price.value,
			rent.price.value, machine.distance, rent.price.packageIndex)

		expect(finalValue.total).toBe(70)
		expect(finalValue.freight).toBe(20)
		expect(finalValue.subTotal).toBe(50)
	})

	test('Calcular com Frete ultrapassando o mínimo', function() {
		var machine = buildMachineDaily(true, true)
		var rent = buildRent()
		var finalValue = calculateRent.calculateRentValue(machine, rent.date.start, rent.date.end, rent.price.value,
			rent.price.value, machine.distance, rent.price.packageIndex)

		expect(finalValue.total).toBe(100)
		expect(finalValue.freight).toBe(50)
		expect(finalValue.subTotal).toBe(50)
	})
})

describe('Testando Tipo ' + calculateRent.typesPricing.MACHINE_HOUR, function () {
	test('Calcular sem Frete', function() {
		var machine = buildMachineHour()
		var rent = buildRent()
		var finalValue = calculateRent.calculateRentValue(machine, rent.date.start, rent.date.end, rent.price.value,
			rent.price.value, machine.distance, rent.price.packageIndex)

		expect(finalValue.total).toBe(80)
		expect(finalValue.freight).toBe(0)
		expect(finalValue.subTotal).toBe(80)
	})

	test('Calcular com Frete Sem atingir o mínimo', function() {
		var machine = buildMachineHour(true)
		var rent = buildRent()
		var finalValue = calculateRent.calculateRentValue(machine, rent.date.start, rent.date.end, rent.price.value,
			rent.price.value, machine.distance, rent.price.packageIndex)

		expect(finalValue.total).toBe(100)
		expect(finalValue.freight).toBe(20)
		expect(finalValue.subTotal).toBe(80)
	})

	test('Calcular com Frete ultrapassando o mínimo', function() {
		var machine = buildMachineHour(true, true)
		var rent = buildRent()
		var finalValue = calculateRent.calculateRentValue(machine, rent.date.start, rent.date.end, rent.price.value,
			rent.price.value, machine.distance, rent.price.packageIndex)

		expect(finalValue.total).toBe(130)
		expect(finalValue.freight).toBe(50)
		expect(finalValue.subTotal).toBe(80)
	})
})

describe('Testando Tipo ' + calculateRent.typesPricing.SACKS_PER_HECTARE, function () {
	test('Calcular sem Frete', function() {
		var machine = buildMachineSacks()
		var rent = buildRent()
		var finalValue = calculateRent.calculateRentValue(machine, rent.date.start, rent.date.end, rent.price.value,
			rent.price.value, machine.distance, rent.price.packageIndex)

		expect(finalValue.total).toBe(80)
		expect(finalValue.freight).toBe(0)
		expect(finalValue.subTotal).toBe(80)
	})

	test('Calcular com Frete Sem atingir o mínimo', function() {
		var machine = buildMachineSacks(true)
		var rent = buildRent()
		var finalValue = calculateRent.calculateRentValue(machine, rent.date.start, rent.date.end, rent.price.value,
			rent.price.value, machine.distance, rent.price.packageIndex)

		expect(finalValue.total).toBe(80)
		expect(finalValue.freight).toBe(20)
		expect(finalValue.subTotal).toBe(80)
	})

	test('Calcular com Frete ultrapassando o mínimo', function() {
		var machine = buildMachineSacks(true, true)
		var rent = buildRent()
		var finalValue = calculateRent.calculateRentValue(machine, rent.date.start, rent.date.end, rent.price.value,
			rent.price.value, machine.distance, rent.price.packageIndex)

		expect(finalValue.total).toBe(80)
		expect(finalValue.freight).toBe(50)
		expect(finalValue.subTotal).toBe(80)
	})
})

describe('Testando Tipo ' + calculateRent.typesPricing.FREIGHT, function () {
	test('Calcular com Frete Sem atingir o mínimo', function() {
		var machine = buildMachineFreight()
		var rent = buildRent()
		var finalValue = calculateRent.calculateRentValue(machine, rent.date.start, rent.date.end, rent.price.value,
			rent.price.value, rent.price.value, rent.price.packageIndex)

		expect(finalValue.total).toBe(20)
		expect(finalValue.freight).toBe(0)
		expect(finalValue.subTotal).toBe(20)
	})

	test('Calcular com Frete ultrapassando o mínimo', function() {
		var machine = buildMachineFreight(true)
		var rent = buildRent()
		var finalValue = calculateRent.calculateRentValue(machine, rent.date.start, rent.date.end, rent.price.value,
			rent.price.value, rent.price.value, rent.price.packageIndex)

		expect(finalValue.total).toBe(80)
		expect(finalValue.freight).toBe(0)
		expect(finalValue.subTotal).toBe(80)
	})
})

describe('Testando Tipo ' + calculateRent.typesPricing.HECTARE_PACKAGE, function () {
	test('Calcular sem Frete', function() {
		var machine = buildMachineHectarePackage()
		var rent = buildRent()
		var finalValue = calculateRent.calculateRentValue(machine, rent.date.start, rent.date.end, rent.price.value,
			rent.price.value, machine.distance, rent.price.packageIndex)

		expect(finalValue.total).toBe(160)
		expect(finalValue.freight).toBe(0)
		expect(finalValue.subTotal).toBe(160)
	})

	test('Calcular com Frete Sem atingir o mínimo', function() {
		var machine = buildMachineHectarePackage(true)
		var rent = buildRent()
		var finalValue = calculateRent.calculateRentValue(machine, rent.date.start, rent.date.end, rent.price.value,
			rent.price.value, machine.distance, rent.price.packageIndex)

		expect(finalValue.total).toBe(180)
		expect(finalValue.freight).toBe(20)
		expect(finalValue.subTotal).toBe(160)
	})

	test('Calcular com Frete ultrapassando o mínimo', function() {
		var machine = buildMachineHectarePackage(true, true)
		var rent = buildRent()
		var finalValue = calculateRent.calculateRentValue(machine, rent.date.start, rent.date.end, rent.price.value,
			rent.price.value, machine.distance, rent.price.packageIndex)

		expect(finalValue.total).toBe(210)
		expect(finalValue.freight).toBe(50)
		expect(finalValue.subTotal).toBe(160)
	})
})

function buildRent() {
	return {
		date: {
			start: new Date(2017, 10, 11),
			end: new Date(2017, 10, 15)
		},
		price: {
			value: 8,
			packageIndex: 0
		}
	}
}

function buildMachineDaily(isFreight, freightGreaterThanMinimum) {
	var machine = {
		price: {
			pricing: calculateRent.typesPricing.DAILY,
			value: 10
		}
	}

	if (isFreight) {
		machine.distance = 5
		machine.price.freight = {
			minimum: 20,
			value_km: 1
		}
	}

	if (freightGreaterThanMinimum) machine.price.freight.value_km = 10

	return machine
}

function buildMachineHour(isFreight, freightGreaterThanMinimum) {
	var machine = {
		price: {
			pricing: calculateRent.typesPricing.MACHINE_HOUR,
			value: 10
		}
	}

	if (isFreight) {
		machine.distance = 5
		machine.price.freight = {
			minimum: 20,
			value_km: 1
		}
	}

	if (freightGreaterThanMinimum) machine.price.freight.value_km = 10

	return machine
}

function buildMachineSacks(isFreight, freightGreaterThanMinimum) {
	var machine = {
		price: {
			pricing: calculateRent.typesPricing.SACKS_PER_HECTARE,
			value: 10
		}
	}

	if (isFreight) {
		machine.distance = 5
		machine.price.freight = {
			minimum: 20,
			value_km: 1
		}
	}

	if (freightGreaterThanMinimum) machine.price.freight.value_km = 10

	return machine
}

function buildMachineHectarePackage(isFreight, freightGreaterThanMinimum) {
	var machine = {
		price: {
			pricing: calculateRent.typesPricing.HECTARE_PACKAGE,
			value: 10,
			hectare_packages: [{
				value: 20
			}]
		}
	}

	if (isFreight) {
		machine.distance = 5
		machine.price.freight = {
			minimum: 20,
			value_km: 1
		}
	}

	if (freightGreaterThanMinimum) machine.price.freight.value_km = 10

	return machine
}


function buildMachineFreight(freightGreaterThanMinimum) {
	var machine = {
		price: {
			pricing: calculateRent.typesPricing.FREIGHT,
			value: 10,
			freight: {
				minimum: 20,
				value_km: 1
			}
		}
	}

	if (freightGreaterThanMinimum) machine.price.freight.value_km = 10

	return machine
}
