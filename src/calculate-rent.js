var typesPricing = {
	DAILY: "DAILY",
	SACKS_PER_HECTARE: "SACKS_PER_HECTARE",
	MACHINE_HOUR: "MACHINE_HOUR",
	HECTARE_PACKAGE: "HECTARE_PACKAGE",
	FREIGHT: "FREIGHT"
}

function calculateRentValue(machine, startDate, endDate, totalSacks, totalHours, distance, packageIndex) {
	var resp = {}

	if (machine.price.pricing === typesPricing.DAILY) {
		resp.subTotal = calculateValueMachineDaily(machine, startDate, endDate)
	}

	if (machine.price.pricing === typesPricing.SACKS_PER_HECTARE) {
		if (totalSacks != null) {
			resp.subTotal = calculateValueMachineSacksPerHectare(machine, totalSacks)
		} else resp.subTotal = 0
	}

	if (machine.price.pricing === typesPricing.MACHINE_HOUR) {
		if (totalHours != null) resp.subTotal = calculateValueMachineHours(machine, totalHours)
		else resp.subTotal = 0
	}

	if (machine.price.pricing === typesPricing.FREIGHT) {
		resp.subTotal = calculateFreightMachine(machine, distance)
	}

	if (machine.price.pricing === typesPricing.HECTARE_PACKAGE) {
		if (totalSacks != null && packageIndex != null) {
			resp.subTotal = calculateValueMachineHectarePackage(machine, totalSacks, packageIndex)
		} else resp.subTotal = 0
	}

	if (machine.price.freight && machine.price.pricing !== typesPricing.FREIGHT) {
		resp.freight = calculateFreightMachine(machine, distance)
	} else resp.freight = 0

	if (resp.freight && machine.price.pricing !== typesPricing.SACKS_PER_HECTARE) {
		resp.total = resp.subTotal + resp.freight
	} else resp.total = resp.subTotal

	return resp
}

function calculateValueMachineDaily(machine, startDate, endDate) {
	var diference = Math.abs(startDate - endDate) // diferença em milésimos e positivo
	var millisecondsADay = 1000 * 60 * 60 * 24 // milésimos correspondente a um dia
	var totalDays = Math.round(diference/millisecondsADay) + 1 // valor total de dias arredondado

	return machine.price.value * totalDays
}

function calculateFreightMachine(machine, distance) {
	var distance = String(distance)
	var value = 0

	if (distance.indexOf("km") !== -1) {
		if (distance.indexOf(",") !== -1) {
			distance = distance.replace(",", ".")
			distance = parseFloat(distance)
		}
	}

	distance = Math.ceil(distance)
	value = distance * machine.price.freight.value_km

	return value < machine.price.freight.minimum ? machine.price.freight.minimum : value
}

function calculateValueMachineSacksPerHectare(machine, totalSacks) {
	return machine.price.value * totalSacks
}

function calculateValueMachineHours(machine, totalHours) {
	return machine.price.value * totalHours
}

function calculateValueMachineHectarePackage(machine, totalSacks, packageIndex) {
	var pack = machine.price.hectare_packages[packageIndex]
	if (!pack) return 0

	return pack.value * totalSacks
}

module.exports = {
	calculateRentValue: calculateRentValue,
	typesPricing: typesPricing
}
